# SPDX-License-Identifier: AGPL-3.0-or-later
# SPDX-License-URL: https://spdx.org/licenses/AGPL-3.0-or-later.html
#
# Caterer: Serve apps or complete desktops from cloud
# Copyright (C) 2022 Rebhu Computing Private Limited
#
# Author: Chintan Mishra <chintan@rebhu.com>
